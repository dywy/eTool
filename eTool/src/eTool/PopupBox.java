package eTool;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PopupBox {

	static String answer;

	public static String display(String title, String message, String lbLabel, String rbLabel) {
		Stage window = new Stage();

		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle(title);
		window.setMinWidth(200);

		Label labelMessage = new Label(message);
		
				// Create leftButton and define action
		Button leftButton = new Button(lbLabel);
		leftButton.setOnAction(e -> {
			answer = "LB";
			window.close();
		});

		// Create rightButton and define action
		Button rightButton = new Button(rbLabel);
		rightButton.setOnAction(e -> {
			answer = "RB";
			window.close();
		});
		
		
		
		HBox buttonsBox = new HBox(10);
		buttonsBox.setPadding(new Insets(10,10,10,10));
		buttonsBox.getChildren().addAll(leftButton, rightButton);
		buttonsBox.setAlignment(Pos.CENTER);

		VBox messageBox = new VBox(10);
		messageBox.setPadding(new Insets(10,10,10,10));
		messageBox.getChildren().addAll(labelMessage);
		messageBox.setAlignment(Pos.CENTER);

		BorderPane layout = new BorderPane();
		layout.setPadding(new Insets(10,10,10,10));
		layout.setCenter(messageBox);
		layout.setBottom(buttonsBox);
		
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
		
		return(answer);

	}

}

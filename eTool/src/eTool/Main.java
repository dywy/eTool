package eTool;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;


public class Main extends Application{
	
	Button closeButton;
	
	
	public static void main (String[] args){
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("HAAS-Mondomix Engineering Tool Kit" );
		
		primaryStage.setOnCloseRequest(e -> closeProgram(primaryStage));
		
		closeButton = new Button("Close");
		closeButton.setOnAction(e -> {
			e.consume();
			closeProgram(primaryStage);
		});
			
		
		StackPane layout = new StackPane();
		layout.getChildren().add(closeButton);
		
		Scene scene = new Scene(layout, 800,600);
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}
	
	private void closeProgram(Stage stage){
		if(PopupBox.display("Please confirm", "Really Quit?", "Yes", "No")=="LB")
			stage.close();
	}

}
